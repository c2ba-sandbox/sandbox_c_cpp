#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <vector>

#include <utils/utils.hpp>

template <typename T> auto partition(T *begin, T *end) {
  // Probably not the most efficient way of partitioning but it works
  size_t smaller_count = 0;
  for (auto ptr = begin; ptr < end; ++ptr) {
    // Count number of elements smaller than the first one
    if (*ptr < *begin) {
      ++smaller_count;
    }
  }
  // Final position of the first element: after all smaller elements
  const auto pivot_ptr = begin + smaller_count;

  // Put it here
  std::swap(*begin, *pivot_ptr);

  // Put any element before or after the pivor position depending on relative
  // order
  auto next_left = begin;
  auto next_right = pivot_ptr + 1;
  while (next_left < pivot_ptr && next_right < end) {
    if (*next_left < *pivot_ptr) {
      ++next_left;
    } else if (*next_right > *pivot_ptr) {
      ++next_right;
    } else {
      std::swap(*next_left, *next_right);
      ++next_left;
      ++next_right;
    }
  }

  return pivot_ptr;
}

template <typename T> auto quick_sort(T *begin, T *end) {
  if (end <= begin) {
    return;
  }
  const auto pivot = partition(begin, end);

  if (pivot == end) {
    return;
  }

  quick_sort(begin, pivot);
  quick_sort(pivot + 1, end);
}

int main(int argc, const char *argv[]) {
  auto numbers = random_vector(argc > 1 ? std::atoll(argv[argc - 1]) : 1 << 24);

  if (numbers.size() < 32) {
    std::cout << "Input: " << numbers << std::endl;
  }

  {
    const auto timer = timed_scope("quick_sort");
    quick_sort(numbers.data(), numbers.data() + numbers.size());
  }

  if (numbers.size() < 32) {
    std::cout << "Output: " << numbers << std::endl;
  }
  std::cout << std::is_sorted(std::begin(numbers), std::end(numbers))
            << std::endl;

  return 0;
}