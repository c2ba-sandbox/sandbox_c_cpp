#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <vector>

#include <utils/utils.hpp>

int main(int argc, const char *argv[]) {
  auto numbers = random_vector(argc > 1 ? std::atoll(argv[argc - 1]) : 1 << 13);

  if (numbers.size() < 32) {
    std::cout << "Input: " << numbers << std::endl;
  }

  {
    const auto timer = timed_scope("bubble_sort");
    auto done = false;
    while (!done) {
      done = true;
      for (auto i = 1ull; i < numbers.size(); ++i) {
        if (numbers[i - 1] > numbers[i]) {
          std::swap(numbers[i - 1], numbers[i]);
          done = false;
        }
      }
    }
  }

  if (numbers.size() < 32) {
    std::cout << "Output: " << numbers << std::endl;
  }
  std::cout << std::is_sorted(std::begin(numbers), std::end(numbers))
            << std::endl;

  return 0;
}