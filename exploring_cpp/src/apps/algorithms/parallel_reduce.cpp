#include <algorithm>
#include <iostream>
#include <thread>

#include <utils/utils.hpp>

int main(int argc, const char *argv[]) {
  if (argc <= 1) {
    std::cerr << "Usage: " << argv[0] << " <count>" << std::endl;
    return 1;
  }

  const size_t count = 1ull << std::atoll(argv[1]);
  std::cout << "count = " << count << std::endl;

  const auto values = [&]() {
    const auto _timer = timed_scope("shuffle");
    return quick_random_vector(count);
  }();

  if (values.size() < 32) {
    std::cout << "Input: " << values << std::endl;
  }

  const auto init = 0ull;
  const auto max_op = [](auto &&lhs, auto &&rhs) { return std::max(lhs, rhs); };
  const auto add_op = [](auto &&lhs, auto &&rhs) { return lhs + rhs; };

  const auto op = max_op;

  const auto compute = [&op](auto begin, const auto end, auto init) {
    auto current = init;
    while (begin < end) {
      current = op(*begin, current);
      ++begin;
    }
    return current;
  };

  {
    const auto _timer = timed_scope("sequential");
    const auto result = compute(begin(values), end(values), init);
    std::cout << "Result = " << result << std::endl;
  }

  {
    const auto _timer = timed_scope("parallel_1");
    // This version spawn threads, each one take numbers and compute.
    // All results are merged at the end
    const auto thread_count = std::thread::hardware_concurrency();
    std::cout << "Number of threads = " << thread_count << std::endl;
    const auto chunk_size = values.size() / thread_count;
    std::remove_const_t<decltype(values)> results(thread_count);

    std::vector<std::thread> threads;
    for (auto i = 0ull; i < thread_count; ++i) {
      threads.emplace_back([&, thread_idx = i]() {
        results[thread_idx] =
            compute(begin(values) + thread_idx * chunk_size,
                    begin(values) + (thread_idx + 1) * chunk_size, init);
      });
    }
    for (auto i = 0ull; i < thread_count; ++i) {
      threads[i].join();
    }
    auto result = compute(begin(results), end(results), init);
    result =
        compute(begin(values) + thread_count * chunk_size, end(values), result);
    std::cout << "Result = " << result << std::endl;
  }

  return 0;
}