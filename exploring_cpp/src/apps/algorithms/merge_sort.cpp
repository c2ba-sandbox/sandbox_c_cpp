#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <cstdlib>
#include <execution>
#include <iostream>
#include <stack>
#include <thread>
#include <tuple>
#include <vector>

#include <utils/utils.hpp>

template <typename T> auto merge(T *begin, T *end, T *pivot, T *buffer) {
  auto out_ptr = buffer;
  auto ptr1 = begin;
  auto ptr2 = pivot;
  while (ptr1 < pivot && ptr2 < end) {
    if (*ptr1 <= *ptr2) {
      *out_ptr++ = *ptr1;
      ++ptr1;
    } else {
      *out_ptr++ = *ptr2;
      ++ptr2;
    }
  }
  if (ptr1 < pivot) {
    std::copy(ptr1, pivot, out_ptr);
  } else if (ptr2 < end) {
    std::copy(ptr2, end, out_ptr);
  }
  std::copy(buffer, buffer + (end - begin), begin);
}

template <typename T> auto merge_sort_rec(T *begin, T *end, T *buffer) {
  const auto count = end - begin;
  if (count == 1) {
    return;
  }
  const auto pivot = begin + count / 2;
  merge_sort_rec(begin, pivot, buffer);
  merge_sort_rec(pivot, end, buffer + count / 2);

  merge(begin, end, pivot, buffer);
}

template <typename T> auto merge_sort(T *begin, T *end) {
  std::vector<T> buffer(end - begin);
  merge_sort_rec(begin, end, buffer.data());
}

template <typename T> auto iterative_merge_sort(T *begin, T *end) {
  std::vector<T> buffer(end - begin);
  const auto count = end - begin;
  auto ranges_size = 1;
  while (ranges_size < count) {
    // std::cout << "N = " << N << std::endl;
    // std::cout << "ranges_size = " << ranges_size << std::endl;
    const auto range_count = 1 + count / ranges_size;
    const auto merge_count = range_count / 2;
    for (auto i = 0; i < merge_count; ++i) {
      const auto r1_begin = std::min(begin + i * 2 * ranges_size, end);
      const auto r2_begin = std::min(r1_begin + ranges_size, end);
      const auto r2_end = std::min(r2_begin + ranges_size, end);
      if (r2_begin >= end) {
        continue;
      }

      // std::cout << i << " : " << (r2_begin - r1_begin) << " "
      //           << (r1_begin - begin) << " " << (r2_begin - begin) <<
      //           std::endl;
      // print(std::cout, r1_begin, r2_begin) << std::endl;
      // print(std::cout, r2_begin, r2_end) << std::endl;
      merge(r1_begin, r2_end, r2_begin, buffer.data() + i * 2 * ranges_size);
      // print(std::cout, r1_begin, r2_end) << std::endl;
    }
    // print(std::cout, begin, end) << std::endl;
    ranges_size *= 2;
  }
}

template <typename T> auto parallel_merge_sort_iterative(T *begin, T *end) {
  std::vector<T> buffer(end - begin);

  std::vector<std::tuple<T *, T *, T *, T *>> merge_tasks;
  std::atomic<uint64_t> next = 0;
  std::atomic<uint64_t> remaining = 0;

  std::mutex mutex;
  std::condition_variable work_available_cv;

  std::condition_variable work_done_cv;

  std::atomic<uint64_t> thread_done_count = 0;

  bool done = false;
  const auto task = [&](auto thread_idx) {
    while (!done) {
      {
        std::unique_lock<std::mutex> lock{mutex};

        while (!done && !remaining) {
          work_available_cv.wait(lock);
        }
      }

      if (done)
        break;

      ++thread_done_count;

      while (remaining) {
        const uint64_t idx = next++;
        if (idx >= merge_tasks.size()) {
          break;
        }
        auto [r1_begin, r2_end, r2_begin, buff_ptr] = merge_tasks[idx];
        merge(r1_begin, r2_end, r2_begin, buff_ptr);
        --remaining;
      }

      --thread_done_count;
    }
  };

  std::vector<std::thread> threads;
  const auto thread_count = std::thread::hardware_concurrency();
  for (auto i = 0u; i < thread_count; ++i) {
    threads.emplace_back(task, i);
  }
  const auto count = end - begin;
  auto ranges_size = 1;
  while (ranges_size < count) {
    const auto range_count = 1 + count / ranges_size;
    const auto merge_count = range_count / 2;
    for (auto i = 0; i < merge_count; ++i) {
      const auto r1_begin = std::min(begin + i * 2 * ranges_size, end);
      const auto r2_begin = std::min(r1_begin + ranges_size, end);
      const auto r2_end = std::min(r2_begin + ranges_size, end);
      if (r2_begin >= end) {
        continue;
      }
      merge_tasks.emplace_back(r1_begin, r2_end, r2_begin,
                               buffer.data() + i * 2 * ranges_size);
    }

    {
      std::unique_lock<std::mutex> lock{mutex};
      remaining = merge_tasks.size();
      next = 0;
      work_available_cv.notify_all();
    }

    {
      while (remaining > 0) {
      }
      while (thread_done_count > 0) {
      }
      merge_tasks.clear();
    }

    ranges_size *= 2;
  }
  done = true;
  work_available_cv.notify_all();
  for (auto &t : threads) {
    t.join();
  }
}

template <typename T> auto parallel_merge_sort_rec(T *begin, T *end) {
  const auto count = end - begin;
  const auto thread_count = std::thread::hardware_concurrency();
  const auto batch_size = count / thread_count;
  std::vector<T> buffer(end - begin);

  const auto task = [&](auto thread_idx) {
    const auto thread_begin = begin + thread_idx * batch_size;
    const auto thread_end =
        (thread_idx + 1) < thread_count ? thread_begin + batch_size : end;
    merge_sort_rec(thread_begin, thread_end,
                   buffer.data() + thread_idx * batch_size);
  };
  std::vector<std::thread> threads;
  for (auto thread_idx = 0; thread_idx < thread_count; ++thread_idx) {
    threads.emplace_back(task, thread_idx);
  }
  for (auto &t : threads) {
    t.join();
  }

#if MERGE_SINGLETHREAD
  for (auto i = 1; i < thread_count; ++i) {
    const auto local_end = (i + 1) < thread_count
                               ? std::min(begin + (i + 1) * batch_size, end)
                               : end;
    const auto pivot = std::min(begin + i * batch_size, end);
    merge(begin, local_end, pivot, buffer.data());
  }
#else
  const auto merge_task = [&](auto batch_idx) {
    const auto local_begin = begin + batch_idx * batch_size;
    const auto local_end =
        (batch_idx + 2) < thread_count
            ? std::min(begin + (batch_idx + 2) * batch_size, end)
            : end;
    const auto pivot = std::min(begin + (batch_idx + 1) * batch_size, end);
    merge(local_begin, local_end, pivot,
          buffer.data() + batch_idx * batch_size);
  };

  auto big_batch_count = 0;
  const auto big_batch_size = 2 * batch_size;
  for (auto i = 0; i < thread_count; i += 2) {
    if (i + 1 < thread_count) {
      threads[i / 2] = std::thread{merge_task, i};
      big_batch_count += 1;
    }
  }

  for (auto i = 0; i < thread_count; i += 2) {
    if (i + 1 < thread_count) {
      threads[i / 2].join();
    }
  }

  for (auto i = 1; i < big_batch_count; ++i) {
    const auto local_end = (i + 1) < thread_count
                               ? std::min(begin + (i + 1) * big_batch_size, end)
                               : end;
    const auto pivot = std::min(begin + i * big_batch_size, end);
    merge(begin, local_end, pivot, buffer.data());
  }
#endif
}

auto get_input(const char **begin, const char **end) {
  std::vector<uint64_t> v;
  for (auto const *it = begin; it < end; ++it) {
    v.emplace_back(std::atoll(*it));
  }
  return v;
}

template <typename T>
uint32_t merge_sort_rec_no_buffer(T *begin, T *end, T *start, T *final,
                                  uint32_t level = 0, uint32_t index = 0);

int main(int argc, const char *argv[]) {
  const auto orig_numbers =
      (argc <= 2)
          ? random_vector(argc > 1 ? std::atoll(argv[argc - 1]) : 1 << 24)
          : get_input(argv + 1, argv + argc);

  if (orig_numbers.size() < 32) {
    std::cout << "Input: " << orig_numbers << std::endl;
  } else {
    std::cout << "Size of Input: " << orig_numbers.size() << std::endl;
  }

  {
    auto numbers = orig_numbers;
    {
      const auto timer = timed_scope("std::sort");
      std::sort(numbers.data(), numbers.data() + numbers.size());
    }

    if (numbers.size() < 32) {
      std::cout << "Output: " << numbers << std::endl;
    }
    std::cout << std::is_sorted(std::begin(numbers), std::end(numbers))
              << std::endl;
  }

  {
    auto numbers = orig_numbers;
    {
      const auto timer = timed_scope("parallel std::sort");
      std::sort(std::execution::par, numbers.data(),
                numbers.data() + numbers.size());
    }

    if (numbers.size() < 32) {
      std::cout << "Output: " << numbers << std::endl;
    }
    std::cout << std::is_sorted(std::begin(numbers), std::end(numbers))
              << std::endl;
  }

  {
    auto numbers = orig_numbers;
    {
      const auto timer = timed_scope("recursive_merge_sort");
      merge_sort(numbers.data(), numbers.data() + numbers.size());
    }

    if (numbers.size() < 32) {
      std::cout << "Output: " << numbers << std::endl;
    }
    std::cout << std::is_sorted(std::begin(numbers), std::end(numbers))
              << std::endl;
  }

  {
    auto numbers = orig_numbers;
    {
      const auto timer = timed_scope("iterative_merge_sort");
      iterative_merge_sort(numbers.data(), numbers.data() + numbers.size());
    }

    if (numbers.size() < 32) {
      std::cout << "Output: " << numbers << std::endl;
    }
    std::cout << std::is_sorted(std::begin(numbers), std::end(numbers))
              << std::endl;
  }

  {
    auto numbers = orig_numbers;
    {
      const auto timer = timed_scope("parallel_merge_sort_rec");
      parallel_merge_sort_rec(numbers.data(), numbers.data() + numbers.size());
    }

    if (numbers.size() < 32) {
      std::cout << "Output: " << numbers << std::endl;
    }
    std::cout << std::is_sorted(std::begin(numbers), std::end(numbers))
              << std::endl;
  }

  if (false) {
    auto numbers = orig_numbers;
    {
      const auto timer = timed_scope("merge_sort");
      merge_sort_rec_no_buffer(numbers.data(), numbers.data() + numbers.size(),
                               numbers.data(), numbers.data() + numbers.size());
    }

    if (numbers.size() < 32) {
      std::cout << "Output: " << numbers << std::endl;
    }
    std::cout << std::is_sorted(std::begin(numbers), std::end(numbers))
              << std::endl;
  }

  return 0;
}

template <typename T> void mirror(T *begin, T *end) {
  while (begin < end) {
    std::swap(*begin, *(end - 1));
    ++begin;
    --end;
  }
}

// Complicated version where I try without a temporary buffer
// Not working, maybe impossible in O(n.log(n))
template <typename T>
auto merge_sort_rec_no_buffer(T *begin, T *end, T *start, T *final,
                              uint32_t level, uint32_t index) {
  const auto count = end - begin;
  if (count == 1) {
    return index + 1;
  }

  // std::cout << std::endl;
  // std::cout << "before level = " << level << " index = " << index <<
  // std::endl;
  const auto pivot = begin + count / 2;

  // std::cout << "begin = " << (begin - start) << std::endl;
  // std::cout << "pivot = " << (pivot - start) << std::endl;
  // std::cout << "end = " << (end - start) << std::endl;

  // print(std::cout, start, final) << std::endl;
  // print(std::cout, begin, pivot) << "| ";
  // print(std::cout, pivot, end) << std::endl;

  const auto print_indent = [level]() -> auto & {
    for (auto i = 0u; i < level; ++i) {
      std::cout << "\t";
    }
    return std::cout;
  };

  print_indent() << "- enter level " << level << " index " << index << " -> "
                 << std::endl;
  print(print_indent(), begin, pivot) << "| ";
  print(std::cout, pivot, end) << std::endl;

  uint32_t next_index = merge_sort_rec_no_buffer(begin, pivot, start, final,
                                                 level + 1, index + 1);
  next_index =
      merge_sort_rec_no_buffer(pivot, end, start, final, level + 1, next_index);

  print_indent() << "- mid level " << level << " index " << index << " -> "
                 << std::endl;
  print(print_indent(), begin, pivot) << "| ";
  print(std::cout, pivot, end) << std::endl;
  print_indent() << ((std::is_sorted(begin, pivot) == 1) ? "TRUE" : "FALSE")
                 << " | "
                 << ((std::is_sorted(pivot, end) == 1) ? "TRUE" : "FALSE")
                 << std::endl;

  // std::cout << "during level = " << level << " index = " << index <<
  // std::endl; std::cout << std::is_sorted(begin, pivot) << std::endl;
  // std::cout << std::is_sorted(pivot, end) << std::endl;
  // print(std::cout, begin, pivot) << "| ";
  // print(std::cout, pivot, end) << std::endl;

  // print(std::cout, start, final) << std::endl;

  auto ptr1 = begin;
  auto ptr2 = pivot;
  auto ptr3 = pivot;

  const auto TEST_INDEX =
      std::getenv("TEST_INDEX") ? std::atoll(std::getenv("TEST_INDEX")) : -1;

  if (index == TEST_INDEX) {
    std::cout << "-----" << std::endl;
    print(std::cout, begin, ptr1) << "| ";
    print(std::cout, ptr1, ptr3) << "| ";
    print(std::cout, ptr3, ptr2) << "| ";
    print(std::cout, ptr2, end) << std::endl;

    std::cout << std::is_sorted(begin, pivot) << " | "
              << std::is_sorted(pivot, end) << std::endl;
  }

  int i = 0;
  while (ptr1 < ptr2 && ptr2 < end) {
    if (ptr1 == ptr3) {
      ptr3 = ptr2;
      if (index == TEST_INDEX) {
        std::cout << "put back ptr3 to ptr2" << std::endl;
      }
    }

    if (index == TEST_INDEX) {
      std::cout << "enter step " << i << std::endl;
      std::cout << "\t - "
                << " ptr1 = " << (ptr1 - begin) << " " << *ptr1 << std::endl;
      std::cout << "\t - "
                << " ptr2 = " << (ptr2 - begin) << " " << *ptr2 << std::endl;
      std::cout << "\t - "
                << " ptr3 = " << (ptr3 - begin) << " " << *ptr3 << std::endl;
      print(std::cout, begin, ptr1) << "| ";
      print(std::cout, ptr1, ptr3) << "| ";
      print(std::cout, ptr3, ptr2) << "| ";
      print(std::cout, ptr2, end) << std::endl;
    }
    if (*ptr1 <= *ptr2) {
      ++ptr1;
      if (index == TEST_INDEX) {
        std::cout << "step " << i << " ptr1 next" << std::endl;
      }
    } else {
      if (*ptr3 < *ptr2) {
        auto ptr3_i = ptr3;
        const auto ptr3_range_size = ptr2 - ptr3;
        for (auto i = 0u; i < ptr3_range_size; ++i) {
          if (ptr1 == ptr3 || *ptr3_i > *ptr2) {
            // HERE WE HAVE AN ISSUE:
            // we created a new range [ptr3, ptr3_i) that is
            // > [ptr3, ptr2), so we need to "swap" then to keep order

            mirror(ptr3_i, ptr2);
            const auto count = ptr3_i - ptr3;

            break;
          }
          std::swap(*ptr1, *ptr3_i);
          ++ptr1;
          ++ptr3_i;
        }
        if (index == TEST_INDEX) {
          std::cout << "step " << i << " BOOM" << std::endl;
        }
      } else {
        std::swap(*ptr1, *ptr2);
        ++ptr1;
        if (ptr2 + 1 < end && *ptr2 > *(ptr2 + 1)) {
          // we broke order of ptr2 range
          ++ptr2;
          // Now [ptr1, ptr3) point to an increasing range
          // [ptr3, ptr2), also, such that [ptr3, ptr2) < [ptr1, ptr3)
          // [ptr2, end) also, such that *ptr2 < *ptr3
          // But we know *ptr3 < *ptr1 so *ptr2 < *ptr1 and next round we will
          // go to the second condition again
          if (index == TEST_INDEX) {
            std::cout << "step " << i << " ptr1 next ptr2 next" << std::endl;
          }
        } else if (ptr2 + 1 < end) {
          if (ptr2 != ptr3) {
            ptr2 = ptr3;
            if (index == TEST_INDEX) {
              std::cout << "step " << i << " ptr1 next reset to pivot"
                        << std::endl;
            }
          }
        }
      }
    }
    if (index == TEST_INDEX) {
      std::cout << "leave step " << i << std::endl;
      std::cout << "\t - "
                << " ptr1 = " << (ptr1 - begin) << " " << *ptr1 << std::endl;
      std::cout << "\t - "
                << " ptr2 = " << (ptr2 - begin) << " " << *ptr2 << std::endl;
      std::cout << "\t - "
                << " ptr3 = " << (ptr3 - begin) << " " << *ptr3 << std::endl;
      print(std::cout, begin, ptr1) << "| ";
      print(std::cout, ptr1, ptr3) << "| ";
      print(std::cout, ptr3, ptr2) << "| ";
      print(std::cout, ptr2, end) << std::endl;
    }
    ++i;
  }

  if (index == TEST_INDEX) {
    std::cout << "end with step " << i << std::endl;
    std::cout << "\t - "
              << " ptr1 = " << (ptr1 - begin) << " " << *ptr1 << std::endl;
    std::cout << "\t - "
              << " ptr2 = " << (ptr2 - begin) << " " << *ptr2 << std::endl;
    std::cout << "\t - "
              << " ptr3 = " << (ptr3 - begin) << " " << *ptr3 << std::endl;
  }

  print_indent() << "- leave level " << level << " index " << index << " -> ";
  print(std::cout, begin, end) << std::endl;

  return next_index;
}