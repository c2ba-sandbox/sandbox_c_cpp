#include <iostream>
#include <numeric>
#include <tuple>
#include <utility>

template <typename T, typename U> constexpr auto compute_a_pair(T &&t, U &&u) {
  return std::pair(std::forward<T>(t), std::forward<U>(u));
}

template <typename... Args> constexpr auto compute_a_tuple(Args &&... args) {
  return std::tuple(std::forward<Args>(args)...);
}

constexpr size_t MAX_SIZE = 16;
auto compute_a_struct(int count) {
  struct {
    int count;
    float values[MAX_SIZE];
  } result = {};

  result.count = count;
  std::iota(result.values, result.values + count, 0.f);

  return result;
}

int main(int argc, const char *argv[]) {
  const auto [a, b] = compute_a_pair(5, "ok");
  std::cout << a << ", " << b << std::endl;
  const auto [x, y, z, w] = compute_a_tuple('t', 0, "to", 7.8);
  std::cout << x << " " << y << " " << z << " " << w << std::endl;

  const auto [count, values] = compute_a_struct(6);
  std::cout << count << std::endl;
  for (auto i = 0; i < MAX_SIZE; ++i) {
    std::cout << values[i] << " ";
  }
  std::cout << std::endl;

  return 0;
}