#include <algorithm>
#include <chrono>
#include <execution>
#include <iostream>
#include <numeric>
#include <random>
#include <unordered_map>
#include <vector>

#include <utils/utils.hpp>

const auto test_sort = [](auto values) {
  std::cout << "sorted = " << is_sorted(begin(values), end(values))
            << std::endl;

  {
    auto copy = values;
    {
      const auto _timer = timed_scope("sort (sequential)");
      sort(begin(copy), end(copy));
    }
    std::cout << "sorted = " << is_sorted(begin(copy), end(copy)) << std::endl;
  }

  {
    auto copy = values;
    {
      const auto _timer = timed_scope("sort (parallel)");
      sort(std::execution::par, begin(copy), end(copy));
    }
    std::cout << "sorted = " << is_sorted(begin(copy), end(copy)) << std::endl;
  }
};

const auto test_all_any_none_of = [](auto values) {
  auto even_only = values;
  auto odd_only_expect_one = values;
  for (auto &x : even_only) {
    x *= 2;
  }
  for (auto &x : odd_only_expect_one) {
    x = 2 * x + 1;
  }
  odd_only_expect_one[size(values) / 2] *= 2;

  const auto is_even = [](auto &&x) { return x % 2 == 0; };
  const auto is_odd = [](auto &&x) { return x % 2 == 1; };

  {
    const auto _timer = timed_scope("all_of (sequential)");
    const auto result = all_of(begin(even_only), end(even_only), is_even);
    std::cout << "all_of is_even = " << result << std::endl;
  }

  {
    const auto _timer = timed_scope("all_of (parallel)");
    const auto result =
        all_of(std::execution::par, begin(even_only), end(even_only), is_even);
    std::cout << "all_of is_even = " << result << std::endl;
  }

  {
    const auto _timer = timed_scope("none_of (sequential)");
    const auto result = none_of(begin(even_only), end(even_only), is_odd);
    std::cout << "none_of is_odd = " << result << std::endl;
  }

  {
    const auto _timer = timed_scope("none_of (parallel)");
    const auto result =
        none_of(std::execution::par, begin(even_only), end(even_only), is_odd);
    std::cout << "none_of is_odd = " << result << std::endl;
  }
  {
    const auto _timer = timed_scope("any_of (sequential)");
    const auto result =
        any_of(begin(odd_only_expect_one), end(odd_only_expect_one), is_even);
    std::cout << "any_of is_even = " << result << std::endl;
  }

  {
    const auto _timer = timed_scope("any_of (parallel)");
    const auto result = any_of(std::execution::par, begin(odd_only_expect_one),
                               end(odd_only_expect_one), is_even);
    std::cout << "any_of is_even = " << result << std::endl;
  }
};

int main(int argc, const char *argv[]) {
  if (argc <= 2) {
    std::cerr << "Usage: " << argv[0] << " <count> <algorithm>" << std::endl;
    return 1;
  }

  const size_t count = 1ull << std::atoll(argv[1]);
  std::cout << "count = " << count << std::endl;

  const auto values = [&]() {
    const auto _timer = timed_scope("shuffle");
    return random_vector(count);
  }();

  const auto algorithm = std::string{argv[2]};

  const auto algo_map =
      std::unordered_map<std::string, void (*)(decltype(values))>{
          {"sort", test_sort}, {"all_any_none", test_all_any_none_of}};

  if (auto it = algo_map.find(algorithm); it != end(algo_map)) {
    (*it).second(values);
    return 0;
  }

  std::cerr << "algorithm " << algorithm << " not found." << std::endl;
  return 1;
}