#include <iostream>
#include <utility>

// For basic sfinae see sfinae_is_class.cpp
// Here is a more advanced sfinae example, using modern C++ tools

template <typename T, typename... Args>
bool constexpr has_func_operator_f(
    int, decltype(std::declval<T>()(std::declval<Args>()...)) *foo = nullptr) {
  return true;
}

template <typename, typename> bool constexpr has_func_operator_f(...) {
  return false;
}

template <typename T, typename... Args>
constexpr bool has_func_operator_v = has_func_operator_f<T, Args...>(0);

void a_function(int) {}

struct a_struct {
  void operator()(int) {}
};

struct another_struct {};

int main(int argc, const char *argv[]) {
  std::cout << has_func_operator_v<decltype(a_function), int> << std::endl;
  std::cout << has_func_operator_v<a_struct, int> << std::endl;
  std::cout << has_func_operator_v<a_struct, float> << std::endl;
  std::cout << has_func_operator_v<float, int> << std::endl;
  std::cout << has_func_operator_v<another_struct, int> << std::endl;
}