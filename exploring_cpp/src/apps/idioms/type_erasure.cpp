#include <iostream>
#include <memory>

struct any {
  struct iany {
    virtual ~iany() {}
  };

  template <typename T> struct tany : public iany {
    T value;
    tany(T &&v) : value{std::move(v)} {}
  };

  any() = default;

  template <typename T>
  any(T &&value) : m_pany{std::make_unique<tany<T>>(std::forward<T>(value))} {}

  template <typename T> T &get() {
    return dynamic_cast<tany<T> &>(*m_pany).value;
  }

  template <typename T> const T &get() const {
    return dynamic_cast<const tany<T> &>(*m_pany).value;
  }

  std::unique_ptr<iany> m_pany;
};

int main(int argc, const char *argv[]) {
  any x = 5;
  std::cout << x.get<int>() << std::endl;

  x = 12.3;

  std::cout << x.get<double>() << std::endl;

  return 0;
}