#include <iostream>
#include <type_traits>

// SFINAE idiom
// Substitution Failure is not an Error
// The compiler won't take into account any template substitution that is
// not valid C++.
// It allows branching at compile time based on properties on types that
// would compile or not.
// Since C++ 14/17, SFINAE is less common in implementations because the
// standard library contains many compile-time tools that can be used with
// if constexpr () {}.

template <typename T> struct my_is_class_1 {
  using is_a_class_t = struct { char a, b; };
  using is_not_a_class_t = char;
  template <typename T>
  static is_a_class_t
  func(int T::*); // Only valid if the type T have pointers to member
  template <typename T> static is_not_a_class_t func(T); // Valid for all types

  // func<T>(0) will be overriden to the most precise and valid function
  // For a type that have pointers to member, it is the one returning
  // is_a_class_t
  static const bool value = sizeof(func<T>(0)) == sizeof(is_a_class_t);
};

template <typename T> struct my_is_class_2 {
  struct is_a_class_t {};
  struct is_not_a_class_t {};
  template <typename T> static is_a_class_t func(int T::*);
  template <typename T> static is_not_a_class_t func(T);

  // for this version we directly test type equality, no need for sizeof tricks
  static const bool value = std::is_same_v<decltype(func<T>(0)), is_a_class_t>;
};

// variable template since C++14
template <typename T> constexpr bool my_is_class_2_v = my_is_class_2<T>::value;

// in C++11 just use std::is_class
template <typename T> using my_is_class_3 = std::is_class<T>;

class a_class {};
struct a_struct {};
using not_a_class = float;

int main(int argc, const char *argv[]) {
  using namespace std;
  cout << my_is_class_1<a_class>::value << endl;
  cout << my_is_class_1<a_struct>::value << endl;
  cout << my_is_class_1<not_a_class>::value << endl;
  cout << endl;

  cout << my_is_class_2<a_class>::value << endl;
  cout << my_is_class_2<a_struct>::value << endl;
  cout << my_is_class_2<not_a_class>::value << endl;
  cout << endl;

  cout << my_is_class_2_v<a_class> << endl;
  cout << my_is_class_2_v<a_struct> << endl;
  cout << my_is_class_2_v<not_a_class> << endl;
  cout << endl;

  return 0;
}