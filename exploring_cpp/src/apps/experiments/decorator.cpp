#include <iostream>
#include <type_traits>

// In this experiment I'm trying to reproduce python's decorators behavior

// The decorator returns a new function that decorates the input function
const auto my_decorator = [](auto &&func) {
  const auto inner = [func = std::move(func)]() {
    std::cout << "enter decorator" << std::endl;

    if constexpr (std::is_same_v<void, decltype(func())>) {
      func();
      std::cout << "exit decorator" << std::endl;
    } else {
      const auto ret = func(); // Next step: try with coroutines
      std::cout << "exit decorator" << std::endl;
      return ret;
    }
  };
  return inner;
};

// Apply the decorator to another function
// equivalent to @my_decorator in python
const auto a_function = my_decorator([]() {
  std::cout << "a_function" << std::endl;
  return 12;
});

int main(int argc, const char *argv[]) {
  std::cout << a_function() << std::endl;
  return 0;
}