#include <iostream>

class Base {
public:
  virtual ~Base() {}

  Base() {
    foo(); // Print Base
  }

  virtual void foo() const { std::cout << "Base" << std::endl; }
};

class Derived : public Base {
public:
  Derived() {
    foo(); // Print Derived
  }

  void foo() const override { std::cout << "Derived" << std::endl; }
};

int main(int argc, char *const argv[]) {
  Derived derived;
  return 0;
}