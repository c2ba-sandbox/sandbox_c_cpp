#include <chrono>
#include <iostream>
#include <numeric>
#include <random>
#include <string>

template <typename T>
std::ostream &operator<<(std::ostream &out, const std::vector<T> &v) {
  for (const auto &item : v) {
    out << item << " ";
  }
  return out;
}

template <typename T>
auto &print(std::ostream &out, const T *begin, const T *end) {
  while (begin < end) {
    out << *begin++ << " ";
  }
  return out;
}

struct timed_scope {
  std::string message;
  std::chrono::system_clock::time_point start =
      std::chrono::system_clock::now();

  timed_scope(const std::string message) : message{std::move(message)} {}

  ~timed_scope() {
    std::cout << message << " time = "
              << std::chrono::duration<double>(
                     std::chrono::system_clock::now() - start)
                     .count()
              << "s" << std::endl;
  }
};

inline auto random_vector(size_t size, int64_t seed = 0) {
  std::vector<uint64_t> values(size);
  std::iota(std::begin(values), std::end(values), uint64_t{0u});
  std::mt19937_64 generator(seed);
  std::shuffle(std::begin(values), std::end(values), generator);
  return values;
}

inline auto quick_random_vector(size_t size, int64_t seed = 0) {
  std::vector<uint64_t> values(size);
  std::mt19937_64 generator(seed);
  std::uniform_int_distribution<uint64_t> distribution;
  for (auto &v : values) {
    v = distribution(generator);
  }
  return values;
}