#include <iostream>

int main(int argc, char **argv) {
  std::cout << "Hello World " << std::endl;
  std::cout << "cpp version " << __cplusplus << std::endl;
  std::cout << "sizeof(void*) = " << sizeof(void *) << std::endl;
  return 0;
}